import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'welcome',
    pathMatch: 'full'
  },
  { path: 'welcome', loadChildren: './public/welcome/welcome.module#WelcomePageModule' },
  { path: 'login', loadChildren: './public/login/login.module#LoginPageModule' },
  { path: 'home', loadChildren: './private/home/home.module#HomePageModule' },
  { path: 'reservations', loadChildren: './private/reservations/reservations.module#ReservationsPageModule' },
  { path: 'buildings', loadChildren: './private/reservations/buildings/buildings.module#BuildingsPageModule' },
  { path: 'categories', loadChildren: './private/reservations/buildings/categories/categories.module#CategoriesPageModule' },
  { path: 'sections', loadChildren: './private/reservations/buildings/categories/sections/sections.module#SectionsPageModule' },
  { path: 'places', loadChildren: './private/reservations/buildings/categories/sections/places/places.module#PlacesPageModule' },
  { path: 'floormap', loadChildren: './private/reservations/buildings/categories/sections/places/floormap/floormap.module#FloormapPageModule' },
  { path: 'place', loadChildren: './private/reservations/buildings/categories/sections/places/place/place.module#PlacePageModule' },
  { path: 'reserve', loadChildren: './private/reservations/reserve/reserve.module#ReservePageModule' },
  { path: 'unreserve', loadChildren: './private/reservations/unreserve/unreserve.module#UnreservePageModule' },
  { path: 'checkin', loadChildren: './private/reservations/checkin/checkin.module#CheckinPageModule' },
  { path: 'checkout', loadChildren: './private/reservations/checkout/checkout.module#CheckoutPageModule' },
  { path: 'about', loadChildren: './private/about/about.module#AboutPageModule' },
  { path: 'help', loadChildren: './private/help/help.module#HelpPageModule' },
  { path: 'peoples', loadChildren: './private/peoples/peoples.module#PeoplesPageModule' },
  { path: 'people', loadChildren: './private/peoples/people/people.module#PeoplePageModule' },
  { path: 'place-map', loadChildren: './private/reservation/buildings/categories/sections/places/place-map/place-map.module#PlaceMapPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
