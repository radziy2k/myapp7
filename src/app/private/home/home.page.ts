import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController,MenuController } from '@ionic/angular';
import { ApiService } from '../../util/api.service';
import { SpinnerService } from '../../util/spinner.service';
import { ToastService } from '../../util/toast.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  userData : any;// {id:"",username:"",fullname:"",contact:"",token:""};  
  // userBookingData: any;

  responseData: any;

  status: any;//0=empty,1=reserve,2=checkin

  detail= {id:0,name:"",datetime:"",expired:"",start:"",end:""};

  history: any;
  header: any;

  reserveButton: any;
  releaseReserveButton: any;
  checkinButton: any;
  checkoutButton: any;

  popupmessage : any;
  message: any;

  constructor(private route: ActivatedRoute,
              private navCtrl: NavController, 
              private menuCtrl: MenuController,
              private api: ApiService,
              private spin: SpinnerService,
              private toast: ToastService,) { 
            
  }

  ngOnInit() {
    this.message = this.route.snapshot.paramMap.get("message")
    this.popupmessage = this.route.snapshot.paramMap.get("popupmessage")
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true); 

    this.spin.present();
    this.userData = JSON.parse(localStorage.getItem('userData'));
    this.api.getCurrentUserInfo(this.userData.id,this.userData.token).then((result) =>{
      
      this.responseData = result;
      this.status = this.responseData.body.detail.status;
      this.history = this.responseData.body.history;
      
      this.detail.name=this.responseData.body.detail.name;
      this.detail.datetime=this.responseData.body.detail.datetime;
      this.detail.expired=this.responseData.body.detail.expired;
      this.detail.start=this.responseData.body.detail.start;
      this.detail.end=this.responseData.body.detail.end;

      localStorage.removeItem('userBookingData')
      localStorage.setItem('userBookingData',JSON.stringify(this.detail))
  
      if(this.status==="0"){//if empty
        this.reserveButton=true;
        this.releaseReserveButton=false;
        this.checkinButton=true;
        this.checkoutButton=false;
        if(!this.message)
          this.message="You don't have any reservation!";
      }
      else if(this.status==="1"){//if reserved
        this.reserveButton=false;
        this.releaseReserveButton=true;
        this.checkinButton=true;
        this.checkoutButton=false;
        if(!this.message)
          this.message="You have reservation";
      }
      else if(this.status==="2"){//if checked-in
        this.reserveButton=false;
        this.releaseReserveButton=false;
        this.checkinButton=false;
        this.checkoutButton=true;
        if(!this.message)
          this.message="You have been check-in!";
      }
      console.log(this.message)
      // loading.dismissAll();
      this.spin.dismiss();
  }, (err) => {
      //Connection failed message
      // this.presentToast("Connection issue, please retry later!",10000);
      // loading.dismissAll();
      this.toast.show("Connection issue, please retry later!",10000)
      this.spin.dismiss();
    });

  } 

  refreshme(){
    console.log("refreshme")
    this.navCtrl.navigateRoot(['home',{"test":"testing"}]);
    }

}
