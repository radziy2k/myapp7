import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceMapPage } from './place-map.page';

describe('PlaceMapPage', () => {
  let component: PlaceMapPage;
  let fixture: ComponentFixture<PlaceMapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceMapPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceMapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
