import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnreservePage } from './unreserve.page';

describe('UnreservePage', () => {
  let component: UnreservePage;
  let fixture: ComponentFixture<UnreservePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnreservePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnreservePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
