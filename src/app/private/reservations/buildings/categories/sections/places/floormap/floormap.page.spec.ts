import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FloormapPage } from './floormap.page';

describe('FloormapPage', () => {
  let component: FloormapPage;
  let fixture: ComponentFixture<FloormapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FloormapPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FloormapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
