import { Component,OnInit } from '@angular/core';
import { NavController,Platform,MenuController } from '@ionic/angular';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
  
  slideOpts = {
    //effect: 'flip',
    cubeEffect: {
      slideShadows: false,
    },
    autoplay: {
      delay: 3000,
    },
  
  };

  constructor(private navCtrl : NavController,
              private platform: Platform,
              public menuCtrl: MenuController,
    )
  {
    this.platform.backButton.subscribe(() => {
        //do nothing
      });
  }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false); 
  } 

  login(){
    console.log("go to login page!")
    this.navCtrl.navigateRoot(['login',{"test":"testing"}]);
  }
}
