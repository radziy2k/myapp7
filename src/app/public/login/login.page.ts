import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController,MenuController } from '@ionic/angular';
import { ApiService } from '../../util/api.service';
import { SpinnerService } from '../../util/spinner.service';
import { ToastService } from '../../util/toast.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  test:any;

  resposeData : any;
  userData = {"username":"", "password":""};
  message = null

  constructor(private route: ActivatedRoute,
            private navCtrl: NavController, 
            private menuCtrl: MenuController,
            private api: ApiService,
            private spin: SpinnerService,
            private toast: ToastService,
            ) { 
    }

  ngOnInit() {
    this.test = this.route.snapshot.paramMap.get("test")
    console.log("test: ",this.test)
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false); 
  } 

  doLogin(){
    if(this.userData.username && this.userData.password){
      this.spin.present();
      this.api.login(this.userData.username,this.userData.password).then((result) =>{
        this.resposeData = result;
        // console.log("login:-");
        // console.log(this.resposeData.body);
        if(this.resposeData.body.fullname){
          let userData = {"id":this.resposeData.body.id,"username":this.resposeData.body.username,"fullname":this.resposeData.body.fullname,"contact":this.resposeData.body.contact,"token":this.resposeData.body.token}
          console.log("successs login:",userData);
          localStorage.setItem('userData', JSON.stringify(userData) )
          this.spin.dismiss();
          this.navCtrl.navigateRoot(['home',{"test":"testing"}]);
        }
        else{
          this.spin.dismiss();
          this.toast.show("Please give valid username & password",1000);
          // this.toast.presentToastWithOptions();
        }
      }, (err) => {
        //Connection failed message
        this.spin.dismiss();
        this.toast.show("Connection issue, please retry later!",10000);
        // this.toast.presentToastWithOptions();

      });

    }
    else{
      this.toast.show("Username and password cannot be empty!",1000);
      console.log()
    }
  console.log("username:",this.userData.username)
  console.log("password:",this.userData.password)
  }

}
