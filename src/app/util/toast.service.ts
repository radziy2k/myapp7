import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toast: ToastController) { }

  async show(message,duration) {
    const toast = await this.toast.create({
      message: message,
      showCloseButton: true,
      duration: duration,
      position: 'bottom',
    });
    toast.present();
  }
}
