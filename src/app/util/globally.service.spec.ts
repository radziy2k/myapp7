import { TestBed } from '@angular/core/testing';

import { GloballyService } from './globally.service';

describe('GloballyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GloballyService = TestBed.get(GloballyService);
    expect(service).toBeTruthy();
  });
});
